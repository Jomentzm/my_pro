<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';


$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$asd = $userDetails->getEpin();

// $userEPin = getuser($conn," WHERE uid = ? ",array("epin"),array($_SESSION['uid']),"s");
// $userEPinUpdate = $userEPin[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/addePin.php" />
    <meta property="og:title" content="添加ePin | Q联盟" />
    <title>添加ePin | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/addePin.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <form class="edit-profile-div2" action="utilities/changePasswordFunction.php" method="POST"> -->
    <!-- <form class="edit-profile-div2" onsubmit="return createAccountValidation();" action="utilities/addePinFunction.php" method="POST"> -->
    <form class="edit-profile-div2" onsubmit="return ePinValidation();" action="utilities/addePinFunction.php" method="POST">    
    <!-- <h1 class="username"><?php //echo $userDetails->getUsername();?></h1> -->

    <!-- <div class="left-div">
        <p class="continue-shopping pointer continue2"><a href="profile.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Back To Profile</a></p>
    </div> -->

    <div class="left-div">
        <p class="continue-shopping pointer continue2"><a href="profile.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > <?php echo _MAINJS_EPIN_BACK ?></a></p>
    </div>


        <h2 class="profile-title">E-Pin</h2>
        
        <?php
        if($asd == "")
        {
        ?>
            <table class="edit-profile-table password-table">
                <tr class="profile-tr">
                    <!-- <td class="profile-td1">Add New E-Pin</td> -->
                    <td class="profile-td1"><?php echo _MAINJS_EPIN_ADD_NEW ?></td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input required name="register_epin" id="register_epin" class="clean edit-profile-input" type="password">
                        <span class="visible-span2">
                            <img src="img/visible.png" class="login-input-icon" alt="View E-Pin" title="View E-Pin" id="editPassword_new_img">
                        </span>
                    </td>
                </tr>            
                <tr class="profile-tr">
                    <!-- <td class="profile-td1">Retype E-Pin</td> -->
                    <td class="profile-td1"><?php echo _MAINJS_EPIN_RETYPE_NEW ?></td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input required name="register_epin_reenter" id="register_epin_reenter" class="clean edit-profile-input"type="password">
                        <span class="visible-span2">
                            <img src="img/visible.png" class="login-input-icon" alt="View E-Pin" title="View E-Pin" id="editPassword_reenter_img">
                        </span>
                    </td>
                </tr>      
            </table>

            <!-- <button class="confirm-btn text-center white-text clean black-button">Confirm</button> -->
            <button class="confirm-btn text-center white-text clean black-button"><?php echo _MAINJS_EPIN_CONFIRM ?></button>
        <?php
        }
        else
        { ?>
           <!-- <h3>E-Pin Added Successfully!</h3> -->
           <h3><?php echo _MAINJS_EPIN_EPIN_ADDED ?></h3>
           <p><br></p>
           <a href="editePin.php" class="confirm-btn text-center white-text clean black-button">Edit E-Pin</a>
           

        <?php
        }
        ?>


            


    </form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Your current password does not match! <br>Please try again.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Your new password must be more than 5 characters";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Password does not match with retype password. <br>Please try again.";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Server problem. <br>Please try again later.";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Password successfully updated!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('register_epin'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('register_epin_reenter'));
</script>

<script>
  function checkIfVariableIsNullOrEmptyString(field,isValidate)
  {
    if(field == null || field == "" || field.length == 0)
    {
      isValidate += 1;
      return isValidate;
    }
    else
    {
      return isValidate;
    }
  }
  function ePinValidation()
  {
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let register_epin = $('#register_epin').val();
    let register_epin_reenter = $('#register_epin_reenter').val();

    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);
  
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_epin,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
    
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_epin_reenter,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    // Second Level Validation
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
      // alert('can continue');
      if(register_epin.length >= 6)
      {
        if(register_epin == register_epin_reenter)
        {        }
        else
        {
          alert('E-Pin does not match ! Please try again ! ');
          event.preventDefault();
        }
      }
      else
      {
        alert('E-Pin must be more than 5 ! Please try again ! ');
        event.preventDefault();
      }
    }
    else
    {
      alert('Please enter all fields required ! ');
      event.preventDefault();
    }
  }
</script>

</body>
</html>