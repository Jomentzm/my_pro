<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/RegisterPointReport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

//$adminRegisterPoint = getRegisterPointReport($conn,"WHERE receive_uid = ?",array("receive_uid"),array($uid),"s");
$adminRegisterPoint = getRegisterPointReport($conn,"WHERE send_uid = ?",array("send_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminRegisterPointReport.php" />
    <meta property="og:title" content="注册分数转让报告 | Q联盟" />
    <title>注册分数转让报告 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminRegisterPointReport.php" />
    <?php include 'css.php'; ?>    
</head>

<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <h1 class="h1-title h1-before-border shipping-h1">注册分数转让报告</h1>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">

            <?php   
            if( !$adminRegisterPoint)
            { ?>
                <h3 class="h1-title h1-before-border shipping-h1">目前没有任何报告</h3>
            <?php
            } 
            else
            {?>

            <h3 class="h1-title h1-before-border shipping-h1">注册分数转让报告 :</h3>
                <table class="shipping-table white-text">
                    <thead>
                        <tr>
                            <th>编号</th>
                            <th>发送人名字</th>
                            <th>分数数额(Pts)</th>
                            <th>接收人名字</th>
                            <th>日期</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                    $conn = connDB();
                    if($adminRegisterPoint)
                    {
                    for($cnt = 0;$cnt < count($adminRegisterPoint) ;$cnt++)
                        {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $adminRegisterPoint[$cnt]->getSendName();?></td>
                                <td><?php echo $adminRegisterPoint[$cnt]->getSendAmount();?></td>
                                <td><?php echo $adminRegisterPoint[$cnt]->getReceiveName();?></td>

                                <td>
                                    <?php $dateCreated = date("Y-m-d",strtotime($adminRegisterPoint[$cnt]->getDateCreated()));echo $dateCreated;?>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    $conn->close();
                    ?>
                    
                    </tbody>
                </table>

            <?php
            } ?>
            

                
        </div>
    </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>