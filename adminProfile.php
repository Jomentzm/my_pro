<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
// require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // $username = rewrite($_POST["update_username"]);
    $fullname = rewrite($_POST["update_fullname"]);
    $phone = rewrite($_POST["update_phone"]);
    $email = rewrite($_POST["update_email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
        echo "INVALID EMAIL FORMAT";
        //$emailErr = "Invalid email format"; 
    }
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];
// $userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
// $userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminProfile.php" />
    <meta property="og:title" content="修改资料 | Q联盟" />
    <title>修改资料 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminProfile.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<!--function to display profile picture-->
<!-- <?php //include 'profilePictureDispalyPartB.php'; ?> -->

<form method="POST" action="utilities/adminEditProfileFunction.php">
<!-- <form method="POST" action="#"> -->

    <h1 class="details-h1">Edit Profile</h1>


    <div class="left50">
    	<p>Name</p>
        <input id="update_fullname" class="clean product-input" type="text" value="<?php echo $userDetails->getFullname();?>" name="update_fullname">
    </div> 
    <div class="left50 right50">
    	<p>Contact</p>
        <input id="update_phone" class="clean product-input" type="text" value="<?php echo $userDetails->getPhone();?>" name="update_phoneno">
    </div>  
	<div class="clear"></div> 
    <div class="width100">
    	<p class="same-padding-p">Email</p>
        <input id="update_email" class="clean product-input" type="email" value="<?php if(isset($userDetails)){echo $userDetails->getEmail();}?>" name="update_email">
    </div>

    <div class="clear"></div>
    <div class="three-btn-container">
        <a href="adminEditPassword.php" class="refund-btn-a white-button three-btn-a"><b>EDIT PASSWORD</b></a>
        <button input type="submit" name="submit" value="Submit" class="shipout-btn-a black-button three-btn-a">CONFIRM</button>
    </div>  

</form>
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Data Update Successfully.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Data.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>