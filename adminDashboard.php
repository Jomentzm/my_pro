<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$aaa = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminDashboard.php" />
    <meta property="og:title" content="管理员概览 | Q联盟"/>
    <title>管理员概览 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminDashboard.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="h1-title h1-before-border">管理层主页</h1>

    <?php
    if($aaa)
        {   //echo count($aaa);
            $aaaa = count($aaa);
        }
        else
        {   $aaaa = 0;  }
    ?>

    <div class="our-div-container">
        <div class="three-div-width">
            <div class="three-white-div1 white-text min-height-blue-box first-blue-box">
                <p class="four-div-p"><b>会员总数</b></p>
                <p class="four-div-p"><span class="bigger-amount"><?php echo $aaaa;?></span></p>
            </div>
            <a href="adminAddReferee.php" class="open-convert convert-button y-btn">添加新会员</a>
        </div>

		<div class="three-div-width middle-white-div1">
            <div class="three-white-div1 white-text min-height-blue-box">
                <p class="four-div-p"><b>运输分数总数额</b></p>
                <p class="four-div-p"><span class="bigger-amount"><?php echo $userDetails->getPoint();?>分</span></p>
            </div>
            <a href="adminTransferPoint.php" class="open-convert convert-button y-btn">转让运输分数</a>
		</div>
 
 		<div class="three-div-width">       
            <div class="three-white-div1 white-text min-height-blue-box">
                <p class="four-div-p"><b>注册分数总数额</b></p>
                <p class="four-div-p"><span class="bigger-amount"><?php echo $userDetails->getBonus();?>分</span></p>
            </div>
            <a href="adminTransferRegisterPoint.php" class="open-convert convert-button y-btn">转让注册分数</a>
		</div>
    </div>
	<div class="clear"></div>


</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功转让注册分数！";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Transfer Points";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }       
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>
