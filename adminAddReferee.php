<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User2.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" />
    <meta property="og:title" content="添加下线 | Q联盟" />
    <title>添加下线 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="h1-title h1-before-border">添加新会员</h1>  
       

        <form  class="edit-profile-div2" action="utilities/adminAddNewRefereeFunction.php" method="POST">
            <table class="edit-profile-table password-table">
                <tr class="profile-tr">
                    <td class="">Name</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Name" id="register_fullname" name="register_fullname" required>
                    </td>
                </tr>

                <!-- <tr class="profile-tr">
                    <td>身份证号码</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="身份证号码" id="register_ic_no" name="register_ic_no" required>
                    </td>
                </tr> -->

                <tr class="profile-tr">
                    <td>Email</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="email" placeholder="Email" id="register_email_user" name="register_email_user" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Address</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Address" id="register_address" name="register_address" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Phone</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Phone" id="register_phone" name="register_phone" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Account Type</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <select name="user_type">
                            <option value="telemarketer">Telemarketer</option>
                            <option value="admin">Admin</option>
                        </select>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Password</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Password" id="register_password" name="register_password" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Confirm Password</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Confirm Password" id="register_retype_password" name="register_retype_password" required>    
                    </td>
                </tr>

            
            
            </table>

    <div class="clear"></div>     

            <button class="confirm-btn text-center white-text clean gold-button"name="refereeButton">注册新会员</button>            
        
        
            <!-- <input required class="login-input password-input clean" type="hidden" id="current_amount" name="current_amount" value="<?php echo $userRefereeLimit;?>">

            <input required class="login-input password-input clean" type="hidden" id="register_password" name="register_password" value="123321">
            <input required class="login-input password-input clean" type="hidden" id="register_retype_password" name="register_retype_password" value="123321"> -->
        </form>

        

        
    

    
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        // $messageType = "Error registering new account.The account already exist";
        $messageType = "新账号的资料已被使用过，请重新输入！";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>



</body>
</html>