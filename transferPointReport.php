<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/TransferPointReport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$uid = $_SESSION['uid'];

$adminList = getTransferPointReport($conn," WHERE receive_uid = ? ",array("receive_uid"),array($uid),"s");
$adminList2 = getTransferPointReport($conn," WHERE send_uid = ? ",array("send_uid"),array($uid),"s");
$userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/transferPointReport.php" />
    <meta property="og:title" content="分数转让报告 | Q联盟" />
    <title>分数转让报告 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/transferPointReport.php" />
    <?php include 'css.php'; ?>    
</head>

<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <h1 class="h1-title h1-before-border shipping-h1">Transfer Point Report</h1> -->
    <h1 class="h1-title h1-before-border shipping-h1">分数转让报告</h1>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        <?php   if($adminList)
          { ?>
            <!-- <h3 class="h1-title h1-before-border shipping-h1">Receive :</h3> -->
            <h3 class="h1-title h1-before-border shipping-h1">分数转入报告 :</h3>
            <table class="shipping-table white-text">
                <thead>
                    <tr>

                        <th>NO.</th>
                        <th>发送人名字</th>
                        <th>接收人名字</th>
                        <th>日期</th>
                        <!-- <th>时间</th> -->
                        <th>分数数额(Pts)</th>

                    </tr>
                </thead>
                <tbody>

                <?php

                    for($cnt = 0;$cnt < count($adminList) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                              <td><?php echo $adminList[$cnt]->getSendName();?></td>
                            <td><?php echo $adminList[$cnt]->getReceiveName();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <!-- <td>
                                <?php //$timeCreated = date("h:m:s",strtotime($adminList[$cnt]->getDateCreated()));echo $timeCreated;?>
                            </td> -->
                            <td><?php echo $adminList[$cnt]->getSendAmount();?></td>

                        </tr>
                        <?php
                    }
                }else { ?>

            <?php    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="clear"></div>


    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        <?php    if($adminList2)
          { ?>
            <!-- <h3 class="h1-title h1-before-border shipping-h1">Send :</h3> -->
            <h3 class="h1-title h1-before-border shipping-h1">分数转出报告 :</h3>

            <table class="shipping-table white-text">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>发送人名字</th>
                        <th>接收人名字</th>
                        <th>日期</th>
                        <!-- <th>时间</th> -->
                        <th>分数数额(Pts)</th>
                        <!-- <th>公司抽佣(Pts)</th> -->
                    </tr>
                </thead>
                <tbody>

                <?php

                    for($cnt = 0;$cnt < count($adminList2) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                              <td><?php echo $adminList2[$cnt]->getSendName();?></td>
                            <td><?php echo $adminList2[$cnt]->getReceiveName();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList2[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <!-- <td>
                                <?php //$timeCreated = date("h:m:s",strtotime($adminList2[$cnt]->getDateCreated()));echo $timeCreated;?>
                            </td> -->
                            <td><?php echo $adminList2[$cnt]->getSendAmount();?></td>
                            <!-- <td><?php //echo $adminList2[$cnt]->getCommission();?></td> -->



                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>


    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        <?php    if( !$adminList && !$adminList2)
          { ?>
            <!-- <h3 class="h1-title h1-before-border shipping-h1">Send :</h3> -->

            <table class="shipping-table white-text">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>发送人名字</th>
                        <th>接收人名字</th>
                        <th>日期</th>
                        <!-- <th>时间</th> -->
                        <th>分数数额(Pts)</th>
                    </tr>
                </thead>
                <tbody>

                <?php

                  ?>

                        <?php


                ?>
                </tbody>
            </table><br><center>  <div class= "width100 oveflow">
                <div class="width20">
                    <div class="white50div">
                目前没有任何报告
              </div>
          </div>
        </div></center><?php } ?>
        </div>
    </div>

    <div class="clear"></div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>
