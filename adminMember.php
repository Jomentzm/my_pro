<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$memberList = getUser($conn," WHERE user_type = ? ORDER BY date_created DESC",array("user_type"),array(1),"i");

$conn->close();

function promptError($msg){
    echo '
        <script>DESC
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminMember.php" />
    <meta property="og:title" content="全体会员 | Q联盟"/>
    <title>全体会员 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminMember.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<h1 class="h1-title h1-before-border shipping-h1">会员</h1>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table white-text">
                <thead>
                    <tr>
                        <th>编号</th>
                        <th>用户名</th>
                        <th>名字</th>
                        <th>电邮</th>
                        <th>加盟日期</th>
                        <th>分数</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                if($memberList)
                {
                    for($cnt = 0;$cnt < count($memberList) ;$cnt++)
                    {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $memberList[$cnt]->getUsername();?></td>
                            <td><?php echo $memberList[$cnt]->getFullname();?></td>
                            <td><?php echo $memberList[$cnt]->getEmail();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($memberList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <td><?php echo $memberList[$cnt]->getPoint();?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
