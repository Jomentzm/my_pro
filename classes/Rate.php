<?php
class Rate {
    /* Member variables */
    var $id,$referralBonus,$commission,$conversionPoint,$chargesWithdraw,$pointVoucher;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $epin
     */
    public function setId($id)
    {
        $this->id = $id;
    }

      /**
       * @return mixed
       */
      public function getreferralBonus()
      {
          return $this->referral_bonus;
      }

      /**
       * @param mixed $epin
       */
      public function setreferralBonus($referralBonus)
      {
          $this->referral_bonus = $referralBonus;
      }

      /**
       * @return mixed
       */
      public function getCommission()
      {
          return $this->commission;
      }

      /**
       * @param mixed $epin
       */
      public function setCommission($commission)
      {
          $this->commission = $commission;
      }

    /**
     * @return mixed
     */
    public function getConversionPoint()
    {
        return $this->conversion_point;
    }

    /**
     * @param mixed $address
     */
    public function setConversionPoint($conversionPoint)
    {
        $this->conversion_point = $conversionPoint;
    }

    // /**
    //  * @return mixed
    //  */
    // public function getWithdrawalNumber()
    // {
    //     return $this->withdrawal_number;
    // }
    //
    // /**
    //  * @param mixed
    //  */
    // public function setWithdrawalNumber($withdrawalNumber)
    // {
    //     $this->withdrawal_number = $withdrawalNumber;
    // }

    /**
     * @return mixed
     */
    public function getChargesWithdraw()
    {
        return $this->charges_withdraw;
    }

    /**
     * @param mixed $uid
     */
    public function setChargesWithdraw($chargesWithdraw)
    {
        $this->charges_withdraw = $chargesWithdraw;
    }

    /**
     * @return mixed
     */
    public function getPointVoucher()
    {
        return $this->point_voucher;
    }

    /**
     * @param mixed $username
     */
    public function setPointVoucher($pointVoucher)
    {
        $this->point_voucher = $pointVoucher;
    }


}

function getRate($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","referral_bonus","commission","conversion_point","charges_withdraw","point_voucher");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"rate");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$referralBonus, $commission, $conversionPoint, $chargesWithdraw, $pointVoucher);

        $resultRows = array();
        while ($stmt->fetch()) {
            $rate = new Rate;
            $rate->setreferralBonus($referralBonus);
            $rate->setCommission($commission);
            $rate->setConversionPoint($conversionPoint);
            $rate->setChargesWithdraw($chargesWithdraw);
            $rate->setPointVoucher($pointVoucher);
            $rate->setId($id);
            
            array_push($resultRows,$rate);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
