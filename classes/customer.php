<?php
class customer_detail{

    var $id,$name,$phone,$email,$status,$remarks,$date_created,$date_update;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail()
    {
        $this->email = $email;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getRemarks()
    {
        return $this->remarks;
    }

    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }

    public function getDate_created()
    {
        return $this->date_created;
    }

    public function setDate_created($date_created)
    {
        $this->date_created = $date_created;
    }

    public function getDate_update()
    {
        return $this->date_update;
    }

    public function setDate_update($date_update)
    {
        $this->date_update = $date_update;
    }

}

function getCustomerDetail($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","phone","email","status","remarks","date_created","date_update");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"customer_detail");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$name,$phone,$email,$status,$remarks,$date_created,$date_update);

        $resultRows = array();
        while ($stmt->fetch()) {
            $customer= new customer_detail;
            $customer->setId($id);
            $customer->setName($name);
            $customer->setPhone($phone);
            $customer->setEmail($email);
            $customer->setStatus($status);
            $customer->setRemarks($remarks);
            $customer->setDate_created($date_created);
            $customer->setDate_update($date_update);

            array_push($resultRows,$customer);
        }    


        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
