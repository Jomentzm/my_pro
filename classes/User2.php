<?php
class User2 {

    var $id,$uid,$name,$email,$address,$phone,$user_type,$password,$salt,$date_created,$date_update;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function getUid()
    {
        return $this->uid;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    public function getUser_type()
    {
        return $this->user_type;
    }

    public function setUser_type($user_type)
    {
        $this->user_type = $user_type;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getDate_created()
    {
        return $this->date_created;
    }

    public function setDate_created($date_created)
    {
        $this->date_created = $date_created;
    }

    public function getDate_update()
    {
        return $this->date_update;
    }

    public function setDate_update($date_update)
    {
        $this->date_update = $date_update;
    }


}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","name","email","address","phone","user_type","password","salt","date_created","date_update");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user2");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$name,$email,$address,$phone,$user_type,$password,$salt,$date_created,$date_update);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user= new User2;
            $user->setId($id);
            $user->setUid($uid);
            $user->setName($name);
            $user->setEmail($email);
            $user->setAddress($address);
            $user->setPhone($phone);
            $user->setUser_type($user_type);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setDate_created($date_created);
            $user->setDate_update($date_update);

            array_push($resultRows,$user);
        }    


        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}