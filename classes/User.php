<?php
class User {
    /* Member variables */
    var $uid,$username,$email,$password,$salt,$phoneNo,$icNo,$countryId,$fullName,$epin,$saltEpin,$emailVerificationCode,$isEmailVerified,$isPhoneVerified,$loginType,
            $userType,$downlineAccumulatedPoints,$canSendNewsletter,$isReferred,$dateCreated,$dateUpdated,
                $address,$birthday,$gender,$picture,
                    $registerDownlineNo,$bonus,$finalAmount,$point;

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getIcNo()
    {
        return $this->icNo;
    }

    /**
     * @param mixed $icNo
     */
    public function setIcNo($icNo)
    {
        $this->icNo = $icNo;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param mixed $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getEpin()
    {
        return $this->epin;
    }

    /**
     * @param mixed $epin
     */
    public function setEpin($epin)
    {
        $this->epin = $epin;
    }

    /**
     * @return mixed
     */
    public function getSaltEpin()
    {
        return $this->saltEpin;
    }

    /**
     * @param mixed $saltEpin
     */
    public function setSaltEpin($saltEpin)
    {
        $this->saltEpin = $saltEpin;
    }

    /**
     * @return mixed
     */
    public function getEmailVerificationCode()
    {
        return $this->emailVerificationCode;
    }

    /**
     * @param mixed $emailVerificationCode
     */
    public function setEmailVerificationCode($emailVerificationCode)
    {
        $this->emailVerificationCode = $emailVerificationCode;
    }

    /**
     * @return mixed
     */
    public function getisEmailVerified()
    {
        return $this->isEmailVerified;
    }

    /**
     * @param mixed $isEmailVerified
     */
    public function setIsEmailVerified($isEmailVerified)
    {
        $this->isEmailVerified = $isEmailVerified;
    }

    /**
     * @return mixed
     */
    public function getisPhoneVerified()
    {
        return $this->isPhoneVerified;
    }

    /**
     * @param mixed $isPhoneVerified
     */
    public function setIsPhoneVerified($isPhoneVerified)
    {
        $this->isPhoneVerified = $isPhoneVerified;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDownlineAccumulatedPoints()
    {
        return $this->downlineAccumulatedPoints;
    }

    /**
     * @param mixed $downlineAccumulatedPoints
     */
    public function setDownlineAccumulatedPoints($downlineAccumulatedPoints)
    {
        $this->downlineAccumulatedPoints = $downlineAccumulatedPoints;
    }

    /**
     * @return mixed
     */
    public function getisReferred()
    {
        return $this->isReferred;
    }

    /**
     * @param mixed $isReferred
     */
    public function setIsReferred($isReferred)
    {
        $this->isReferred = $isReferred;
    }

    /**
     * @return mixed
     */
    public function getCanSendNewsletter()
    {
        return $this->canSendNewsletter;
    }

    /**
     * @param mixed $canSendNewsletter
     */
    public function setCanSendNewsletter($canSendNewsletter)
    {
        $this->canSendNewsletter = $canSendNewsletter;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

        /**
     * @return mixed
     */
    public function getRegisterDownlineNo()
    {
        return $this->registerDownlineNo;
    }

    /**
     * @param mixed $registerDownlineNo
     */
    public function setRegisterDownlineNo($registerDownlineNo)
    {
        $this->registerDownlineNo = $registerDownlineNo;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param mixed $bonus
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
       * @return mixed
       */
      public function getFinalAmount()
      {
          return $this->finalAmount;
      }

      /**
       * @param mixed $finalAmount
       */
      public function setFinalAmount($finalAmount)
      {
          $this->finalAmount = $finalAmount;
      }

      /**
       * @return mixed
       */
      public function getPoint()
      {
          return $this->point;
      }

      /**
       * @param mixed $point
       */
      public function setPoint($point)
      {
          $this->point = $point;
      }



}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("uid","username","email","password","salt","phone_no","ic_no","country_id","full_name","epin","salt_epin","email_verification_code","is_email_verified",
        "is_phone_verified","login_type","user_type","downline_accumulated_points","can_send_newsletter","is_referred","date_created","date_updated",
            "address","birthday","gender","picture_id", 
            "register_downline_no","bonus","final_amount", "point");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($uid, $username, $email, $password, $salt, $phoneNo, $icNo, $countryId, $fullName, $epin ,$saltEpin, $emailVerificationCode,$isEmailVerified,
            $isPhoneVerified, $loginType, $userType, $downlineAccumulatedPoints, $canSendNewsletter, $isReferred, $dateCreated, $dateUpdated,
                $address, $birthday, $gender, $picture, 
                $registerDownlineNo,$bonus,$finalAmount,$point);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setPhoneNo($phoneNo);
            $user->setIcNo($icNo);
            $user->setCountryId($countryId);
            $user->setFullName($fullName);
            $user->setEpin($epin);
            $user->setSaltEpin($saltEpin);
            $user->setEmailVerificationCode($emailVerificationCode);
            $user->setIsEmailVerified($isEmailVerified);
            $user->setIsPhoneVerified($isPhoneVerified);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setDownlineAccumulatedPoints($downlineAccumulatedPoints);
            $user->setCanSendNewsletter($canSendNewsletter);
            $user->setIsReferred($isReferred);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);
            $user->setAddress($address);
            $user->setBirthday($birthday);
            $user->setGender($gender);
            $user->setPicture($picture);
            $user->setRegisterDownlineNo($registerDownlineNo);
            $user->setBonus($bonus);
            $user->setFinalAmount($finalAmount);
            $user->setPoint($point);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createTotalWithdrawAmount(){

  $totaedlWithdrawAmount = 0;

      if($withdrawAmount > 0){

          $totaledWithdrawAmount = $withdrawAmount + $userDetails->getWithdrawAmount();

      }else{

      }


}
