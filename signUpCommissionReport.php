<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/SignUpCommission.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

$userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");

$userSignUp = getReferralHistory($conn,"WHERE referrer_id = ? ORDER BY date_created DESC ",array("referrer_id"),array($uid),"s");

$signUpCommission = getSignUpCommission($conn,"WHERE referrer_id = ? ORDER BY date_created DESC ",array("referrer_id"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/signUpReport.php" />
    <meta property="og:title" content="注册新会员分数报告 | Q联盟" />
    <title>注册新会员分数报告 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/signUpReport.php" />
    <?php include 'css.php'; ?>    
</head>

<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <h1 class="h1-title h1-before-border shipping-h1">注册新会员分数报告</h1>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <div class="overflow-scroll-div">

            <table class="shipping-table white-text">
                <thead>
                    <tr>
                        <th>编号</th>
                        <th>用户名</th>
                        <th>名字</th>
                        <th>分数</th>
                        <th>日期</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = connDB();
                    if($signUpCommission)
                    {
                    for($cnt = 0;$cnt < count($signUpCommission) ;$cnt++)
                        {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $signUpCommission[0]->getReferralName();?></td>
                                <td><?php echo $signUpCommission[0]->getReferralFullname();?></td>
                                <td><?php echo $signUpCommission[0]->getCommission();?></td>
                                <td>
                                    <?php $dateCreated = date("Y-m-d",strtotime($signUpCommission[$cnt]->getDateCreated()));echo $dateCreated;?>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    $conn->close();
                    ?>
                </tbody>



            </table>

        </div>
    </div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>
