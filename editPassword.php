<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/editPassword.php" />
    <meta property="og:title" content="更改密码 | Q联盟" />
    <title>更改密码 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/editPassword.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <form class="edit-profile-div2" action="utilities/changePasswordFunction.php" method="POST">
        <h1 class="username"><?php echo $userDetails->getUsername();?></h1>
        <!-- <h2 class="profile-title">CHANGE PASSWORD</h2> -->
        <h2 class="profile-title">更改密码</h2>
        <table class="edit-profile-table password-table">
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Current Password</td> -->
                <td class="profile-td1">现有密码</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editPassword_current" id="editPassword_current" class="clean edit-profile-input" type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_current_img">
                    </span>
                </td>
            </tr>
        	<tr class="profile-tr">
                <td class="profile-td1">新密码</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editPassword_new" id="editPassword_new" class="clean edit-profile-input" type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_new_img">
                    </span>
                </td>
            </tr>            
        	<tr class="profile-tr">
                <td class="profile-td1">重新输入新密码</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editPassword_reenter" id="editPassword_reenter" class="clean edit-profile-input"type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_reenter_img">
                    </span>
                </td>
            </tr>          
        </table>
        <button class="confirm-btn text-center white-text clean black-button">确认</button>
    </form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "现有密码不匹配  <br>    请再试一遍 ！";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "新密码必须超过5个字符";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "新密码与重新输入新密码不匹配    <br>  请再试一遍 ！";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "服务器问题  <br>  请稍后再试一遍 ！";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "成功更换密码";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
  viewPassword( document.getElementById('editPassword_current_img'), document.getElementById('editPassword_current'));
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('editPassword_new'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('editPassword_reenter'));
</script>

</body>
</html>