<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/customer.php';
// require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getCustomerDetail($conn," WHERE id = ? ",array("id"),array($_SESSION['id']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/customer_detail.php" />
    <meta property="og:title" content="添加下线 | Q联盟" />
    <title>添加下线 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/customer_detail.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="h1-title h1-before-border">Add Customer Detail</h1>

    
        <form  class="edit-profile-div2" action="utilities/addCustomerDetailFunction.php" method="POST">
            <table class="edit-profile-table password-table">
                <tr class="profile-tr">
                    <td class="">Name</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Name" id="register_name" name="register_name" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Phone</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Phone" id="register_phone" name="register_phone" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Email</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="email" placeholder="Email" id="register_email" name="register_email" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Status</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Status" id="register_status" name="register_status" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Remarks</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <textarea rows="5" column="40"class="clean edit-profile-input" type="text" id="remarks" name="remarks" style="border:1px solid White"></textarea>
                    </td>
                </tr>

            
            
            </table>

    <div class="clear"></div>     

            <button class="confirm-btn text-center white-text clean gold-button"name="refereeButton">Confirm</button>            
        
        
           
        </form>




</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
       
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully added new referee ！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Error adding new referee ！";
        }
        
        echo '
        <script>
            putNoticeJavascript("NOTICE !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        // $messageType = "Error registering new account.The account already exist";
        $messageType = "Error registering new account. The account already exist！";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("NOTICE !! ","'.$messageType.'");
    </script>
    ';   
}
?>



</body>
</html>