<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User2.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';


$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/profile.php" />
    <meta property="og:title" content="个人中心 | Q联盟" />
    <title>个人中心 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/profile.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <div class="right-profile-div">
    	<div class="profile-tab width100">
            <a href="#" class="profile-tab-a active-tab-a">个人中心</a>
            <a href="referee.php" class="profile-tab-a">我的组织</a>
        </div>

        <div class="left-profile-div-user">
            <h1 class="name"> <?php echo $userDetails->getName();?> </h1>
        </div>
		<div class="width100 overflow light-grey-div">
            <h2 class="profile-title h2-margin">基本资料</h2>
            <table class="profile-table profile-page-table">
                <!-- <tr class="profile-tr">
                    <td class="profile-td1">身份证</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php //echo $userDetails->getIcNo();?></td>
                </tr> -->
                <tr class="profile-tr">
                    <td class="profile-td1">Name</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $userDetails->getName();?></td>
                </tr>   

                <tr class="profile-tr">
                    <td class="profile-td1">Email</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $userDetails->getEmail();?></td>
                </tr>

                <tr class="profile-tr">
                    <td class="profile-td1">Phone</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $userDetails->getPhone();?></td>
                </tr>

                <tr class="profile-tr">
                    <td class="profile-td1">Address</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $userDetails->getAddress()?></td>
                </tr>

                <div class="w30" style="float:right">
            	<a href="customer_detail.php" class="w50-a">
                    <img src="img/add-referee2.png" class="open-transfer gradient-button" alt="添加会员" title="添加会员"  style="float:right">
                    <p class="w50-p" style="float:right">Add Customer Detail</p>
                </a>
                </div>
 
                
            </table>  	
        </div>
        <!-- <div class="width100 oveflow wallet-big-div profile-big-div">
        	<div class="width50-real">
            	
                    <img src="img/shipped.png" class="profile-icon" alt="运输分数" title="运输分数">
                     <h3 class="white-text">分</h3>
                     <h2 class="gold-text">运输分数</h2>
                   
    


            </div>

            <div class="width50-real width50-real2">
            
                    <img src="img/register-point.png" class="profile-icon" alt="注册分数" title="注册分数">
                    <h3 class="white-text">分</h3>
                    <h2 class="gold-text">注册分数</h2>
         
            </div>
        </div>

        <div class="clear"></div>
		<div class="width100 same-padding some-spacing">
        	<div class="w30"> 
            	<a class="open-transfer w50-a">
                    <img src="img/transfer-point.png" class="gradient-button" alt="转让运输分数" title="转让运输分数">
                    <p class="w50-p">转让运输分数</p>
                </a>
            </div> 
         	<div class="w30 m30">  
            	<a class="open-transfer w50-a">
                 <a href="transferRegisterPoints.php" class="w50-a">
                    <img src="img/trasnfer-pts.png" class="gradient-button" alt="转让注册分数" title="转让注册分数">
                    <p class="w50-p">转让注册分数</p>
                </a>
            </div>           
            
            <div class="w30">
            	<a href="addReferee.php" class="w50-a">
                    <img src="img/add-referee2.png" class="open-transfer gradient-button" alt="添加会员" title="添加会员"  style="float:right">
                    <p class="w50-p" >添加会员</p>
                </a>
            </div>
        </div> -->
    </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户";
        }
        // if($_GET['type'] == 11)
        // {
        //     $messageType = "Picture Update Successfully";
        // }
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "E-Pin Renew Successfully";
        }
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>