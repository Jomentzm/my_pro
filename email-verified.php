<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$uid = null;
$userRows = null;
$conn = connDB();

if(isset($_GET['getVerified']))
{
    $uid = rewrite($_GET['getVerified']);
    echo $uid;
    if(updateDynamicData($conn,"user"," WHERE uid = ? ",array("is_email_verified"),array(1,$uid),"is"))
    {
        header( "refresh:3;url=index.php" );
    }
    else 
    {
        header('Location:index.php');
    }

    // echo $uid;
}
// header( "refresh:5;url=wherever.php" );

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/email-verified.php" />
    <meta property="og:title" content="电邮验证 | Q联盟" />
    <title>电邮验证 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/email-verified.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="success-h1 text-center">
    	Success!
    </h1>
    <p class="success-p text-center">
    	Your email has been successfully verified!
    </p>


</div>
<?php include 'js.php'; ?>

</body>
</html>