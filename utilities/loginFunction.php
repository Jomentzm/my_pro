<?php
if (session_id() == ""){
     session_start();
}

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User2.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        $email = rewrite($_POST['email']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE name = ? ",array("name"),array($email),"s");
        if($userRows)
        {
            $user = $userRows[0];

            // if($user->getisEmailVerified() == 1)
            // {
                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256',$user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    if(isset($_POST['remember-me'])) 
                    {
                        
                        setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                        setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                        // echo 'remember me';
                    }
                    else 
                    {
                        setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                        // echo 'null';
                    }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['user_type'] = $user->getUser_type();
                    

                    if($user->getUser_type() == 1)
                    {
                        header('Location: ../adminDashboard.php');
                    }
                    else
                    {
                        // header('Location: ../profile.php?lang=ch');
                        header('Location: ../profile.php');
                    }
                }
                else 
                {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../index.php?type=11');
                    // promptError("Incorrect email or password");
                    echo("login fail");
                }
            // }
            // else 
            // {
            //         $_SESSION['messageType'] = 1;
            //         header('Location: ../index.php?type=10');
            //    //  promptError("Please confirm your registration inside your email");
            // }

        }
        else
        {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=7');
        //   echo "// no user with this email ";
          //   promptError("This account does not exist");
        }
    }

    $conn->close();
}
?>