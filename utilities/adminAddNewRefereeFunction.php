<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User2.php';
// require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

$uid = $_SESSION['uid'];

function registerNewUser($conn,$uid,$name,$email,$address,$phone,$user_type,$password,$salt)
{
     if(insertDynamicData($conn,"user2",array("uid","name","email","address","phone","user_type","password","salt"),
          array($uid,$name,$email,$address,$phone,$user_type,$password,$salt),"sssssiss") === null)
     {
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
          echo"error 1";
     }
     else true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_fullname = rewrite($_POST['register_fullname']);
     // $register_ic_no = rewrite($_POST['register_ic_no']);
     $register_email_user = $_POST['register_email_user'];
     $register_address = $_POST['register_address'];
     $user_type = rewrite($_POST['user_type']);
     $register_phone = $_POST['register_phone'];
     // $register_username_referrer = $_POST['register_username_referrer'];

     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";
     // echo $register_password."<br>";
     // echo $register_retype_password."<br>";
     // echo $user_type."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {    
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
               $usernameDetails = $usernameRows[0];

               $fullnameRows = getUser($conn," WHERE name = ? ",array("name"),array($_POST['register_fullname']),"s");
               $fullnameDetails = $fullnameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
               $userEmailDetails = $userEmailRows[0];



               if (!$fullnameDetails && !$userEmailDetails && !$usernameDetails)
               {
                    // $register_points = $newUser;  

                    if(registerNewUser($conn,$register_uid,$register_fullname,$register_email_user,$register_address,$user_type,$register_phone_no,$finalPassword,$salt))
                    {
                         // $_SESSION['messageType'] = 2;
                         // header('Location: ../adminDashboard.php?type=1');
                         echo"success";
                    }
               }
               else
               {
                    // header('Location: ../adminAddReferee.php?promptError=1');
                    echo"overlap";
               }
         
          }
          else 
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../adminAddReferee.php?type=5');
               echo"password";
          }
     }
     else 
     {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../adminAddReferee.php?type=5');
          echo"password not equal";
     }   
    
}
else 
{
     header('Location: ../addReferee.php');
}

?>