<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require_once dirname(__FILE__) . '/../vendor/autoload.php';

function sendMailTo(
$verifyUser_debugMode,
$verifyUser_host,
$verifyUser_usernameThatSendEmail,
$verifyUser_password,
$verifyUser_smtpSecure,
$verifyUser_port, 
$verifyUser_sentFromThisEmailName,
$verifyUser_sentFromThisEmail,
$verifyUser_sendToThisEmailName,
$verifyUser_sendToThisEmail,
$verifyUser_isHtml,
$verifyUser_subject,
$verifyUser_body,
$verifyUser_altBody
)
{
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try 
    {
        //Server settings
        if($verifyUser_debugMode != null)
        {
            $mail->SMTPDebug = $verifyUser_debugMode;                                       // To debug output
        }
        
        $mail->isSMTP();                                                                    // Set mailer to use SMTP
        $mail->Host       = $verifyUser_host;                                               // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                                           // Enable SMTP authentication
        $mail->Username   = $verifyUser_usernameThatSendEmail;                              // SMTP username
        $mail->Password   = $verifyUser_password;                                           // SMTP password
        $mail->SMTPSecure = $verifyUser_smtpSecure;                                         // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = $verifyUser_port;                                               // TCP port to connect to
    
        //Recipients
        $mail->setFrom($verifyUser_sentFromThisEmail,$verifyUser_sentFromThisEmailName);    // Mailer is Username
        $mail->addAddress($verifyUser_sendToThisEmail,$verifyUser_sendToThisEmailName);     // Add a recipient
        // $mail->addAddress('hafeezdzeko374@gmail.com');                                   // Name is optional
    
        // Content
        $mail->isHTML($verifyUser_isHtml);                                                  // Set email format to HTML
        $mail->Subject = $verifyUser_subject;
        $mail->Body    = $verifyUser_body;
        $mail->AltBody = $verifyUser_altBody;
    
        $mail->send();
        return 'Message has been sent';
    } 
    catch (Exception $e) 
    {
        return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
}
?>