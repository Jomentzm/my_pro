<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        $userDetails = $userRows[0];
        $sendName = $userDetails->getUsername();

        $currentUsernameSearch = $_POST["username"];
        $existingIC = $userDetails -> getIcNo();
        // $currentPoint = $userDetails -> getUserPoint();
        $currentPoint = $userDetails -> getPoint();

        // $transferAmount = $_POST["transferAmount"];

        // $transferAmount = $_POST["transferAmount"];
        // $charges = $rateDetails -> getChargesWithdraw();
        // $withdrawafterCharge =$withdraw - $charges;

        $transferPoint = $_POST["transferAmount"];
        $charges = 1*(95/100);
        $transferAmount =$transferPoint * $charges;

        $commission =$transferPoint - $transferAmount;

        //   FOR DEBUGGING
        echo "<br>";
        echo $charges."<br>";
        echo $transferAmount."<br>";
        echo $commission."<br>";

        $referralHistorys = getReferralHistory($conn,"WHERE referral_name = ? ",array("referral_name"),array($currentUsernameSearch),"s");

        if ($referralHistorys)
        {
            $getTopReferrerReceiver = $referralHistorys[0]->getTopReferrerId();
            $getReferralIdReceiver = $referralHistorys[0]->getReferralId();
            $userReceiverDetails = getUser($conn,"WHERE uid = ? ",array("uid"),array($getReferralIdReceiver),"s");
            
            if ($userReceiverDetails)
            {
                // $userPoint = $userReceiverDetails[0]->getUserPoint();
                $userPoint = $userReceiverDetails[0]->getPoint();
            }

            $referralHistoryII = getReferralHistory($conn,"WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
            if ($referralHistoryII)
            {
                $getTopReferrerSender = $referralHistoryII[0]->getTopReferrerId();
                $getNameSender = $referralHistoryII[0]->getReferralName();
            }
            $referralHistoryAdmin = getReferralHistory($conn,"WHERE top_referrer_id = ? ",array("top_referrer_id"),array($uid),"s");
            if ($referralHistoryAdmin)
            {
                $getUidAdmin = $referralHistoryAdmin[0]->getTopReferrerId();
            }

            // $currentEnterEpin = $_POST["withdraw_epin_convert"];
            // $dbEpin =  $userDetails->getEpin();
            // $dbSaltEpin =  $userDetails->getSaltEpin();
            // $newEpin_hashed = hash('sha256',$currentEnterEpin);
            $status = 'RECEIVED';
            // $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

                if ($transferAmount <= $currentPoint &&  $currentPoint > 0)
                {
                    // if ($dbEpin == $newEpin_hashed_salt)
                    // {
                        if ($getTopReferrerSender == $getTopReferrerReceiver || $getUidAdmin==$getTopReferrerReceiver)
                        {
                            if ($getNameSender != $currentUsernameSearch)
                            {
                                // $pointNow = $currentPoint - $transferAmount;

                                $pointNow = $currentPoint - $transferPoint;
                                $pointBeingTransfer = $userPoint + $transferAmount;

                                    // if (NewWithdraw($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status))
                                    if (NewTransferPoint($conn,$uid,$sendName,$transferAmount,$commission,$currentUsernameSearch,$getReferralIdReceiver,$status))
                                    {
                                        $_SESSION['messageType'] = 4;
                                        header('Location: ../profile.php?type=1');
                                    }
                                    else
                                    {
                                        $_SESSION['messageType'] = 5;
                                        header('Location: ../profile.php?type=1');
                                    }

                                $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
                                if(!$user)
                                {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                    if($pointNow)
                                    {
                                        array_push($tableName,"point");
                                        array_push($tableValue,$pointNow);
                                        $stringType .=  "s";
                                        // $stringType .=  "i";
                                    }
                                    if(!$pointNow)
                                    {   $point = 0;
                                        array_push($tableName,"point");
                                        array_push($tableValue,$point);
                                        $stringType .=  "s";
                                        // $stringType .=  "i";
                                    }
                                    // if($finalMoney)
                                    // {
                                    //     array_push($tableName,"final_amount");
                                    //     array_push($tableValue,$finalMoney);
                                    //     $stringType .=  "i";
                                    // }

                                    array_push($tableValue,$uid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {   }
                                    else
                                    {   }
                                }
                                else
                                {   }
                                $_SESSION['messageType'] = 4;
                                header('Location: ../profile.php?type=8');

                            }
                            else
                            {
                                $_SESSION['messageType'] = 5;
                                header('Location: ../profile.php?type=3');
                            }

                        }
                        else
                        {
                            $_SESSION['messageType'] = 5;
                            header('Location: ../profile.php?type=2');
                        }

                    // }
                    // else
                    // {
                    //     $_SESSION['messageType'] = 4;
                    //     header('Location: ../profile.php?type=3');
                    // }

                }
                else
                {
                    $_SESSION['messageType'] = 4;
                    header('Location: ../profile.php?type=9');
                }

        }
        else
        {
            $_SESSION['messageType'] = 5;
            header('Location: ../profile.php?type=4');
        }

        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($pointBeingTransfer)
            {
                array_push($tableName,"point");
                array_push($tableValue,$pointBeingTransfer);
                $stringType .=  "s";
            }

            array_push($tableValue,$currentUsernameSearch);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {   }
            else
            {   }
        }
        else
        {   }

    }
    else
    {
        header('Location: ../index.php');
    }

// function NewTransferPoint($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status)
// {
//     if(insertDynamicData($conn,"transfer_point",array("send_uid","send_name","amount","receive_name","receive_uid","status"),
//             array($uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status),"ssssss") === null)
//         {
//             return false;
//         }
//     else
//     {  }
//     return true;
// }

function NewTransferPoint($conn,$uid,$sendName,$transferAmount,$commission,$currentUsernameSearch,$getReferralIdReceiver,$status)
{
    if(insertDynamicData($conn,"transfer_point",array("send_uid","send_name","amount","commission","receive_name","receive_uid","status"),
            array($uid,$sendName,$transferAmount,$commission,$currentUsernameSearch,$getReferralIdReceiver,$status),"sssssss") === null)
        {
            return false;
        }
    else
    {  }
    return true;
}

?>
