<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/RegisterPointReport.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        $userDetails = $userRows[0];
        $sendName = $userDetails->getUsername();

        $currentUsernameSearch = $_POST["username"];
        // $existingIC = $userDetails -> getIcNo();
        // $currentPoint = $userDetails -> getUserPoint();
        $currentPoint = $userDetails -> getBonus();

        $transferAmount = $_POST["transferAmount"];

        //   FOR DEBUGGING
        // echo "<br>";
        // echo $transferAmount."<br>";

        $referralHistorys = getReferralHistory($conn,"WHERE referral_name = ? ",array("referral_name"),array($currentUsernameSearch),"s");

        if ($referralHistorys)
        {
            $getTopReferrerReceiver = $referralHistorys[0]->getTopReferrerId();
            $getReferralIdReceiver = $referralHistorys[0]->getReferralId();
            $userReceiverDetails = getUser($conn,"WHERE uid = ? ",array("uid"),array($getReferralIdReceiver),"s");
            
            if ($userReceiverDetails)
            {
                // $userPoint = $userReceiverDetails[0]->getUserPoint();
                $userPoint = $userReceiverDetails[0]->getBonus();
            }

            $referralHistoryII = getReferralHistory($conn,"WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
            if ($referralHistoryII)
            {
                $getTopReferrerSender = $referralHistoryII[0]->getTopReferrerId();
                $getNameSender = $referralHistoryII[0]->getReferralName();
            }
            $referralHistoryAdmin = getReferralHistory($conn,"WHERE top_referrer_id = ? ",array("top_referrer_id"),array($uid),"s");
            if ($referralHistoryAdmin)
            {
                $getUidAdmin = $referralHistoryAdmin[0]->getTopReferrerId();
            }

            // $currentEnterEpin = $_POST["withdraw_epin_convert"];
            // $dbEpin =  $userDetails->getEpin();
            // $dbSaltEpin =  $userDetails->getSaltEpin();
            // $newEpin_hashed = hash('sha256',$currentEnterEpin);
            // $status = 'RECEIVED';
            // $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

            $status = 'RECEIVED';

                if ($transferAmount <= $currentPoint &&  $currentPoint > 0)
                {
                        if ($getTopReferrerSender == $getTopReferrerReceiver || $getUidAdmin==$getTopReferrerReceiver)
                        {
                            if ($getNameSender != $currentUsernameSearch)
                            {
                                $pointNow = $currentPoint - $transferAmount;
                                $pointBeingTransfer = $userPoint + $transferAmount;

                                    if (adminRegisterPoint($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status))
                                    {
                                        $_SESSION['messageType'] = 1;
                                        //header('Location: ../adminTransferRegisterPoint.php?type=1');
                                        header('Location: ../adminDashboard.php?type=1');
                                    }
                                    else
                                    {
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../adminTransferRegisterPoint.php?type=2');
                                    }

                                $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
                                if(!$user)
                                {
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                    if($pointNow)
                                    {
                                        array_push($tableName,"bonus");
                                        array_push($tableValue,$pointNow);
                                        $stringType .=  "s";
                                    }
                                    if(!$pointNow)
                                    {   $point = 0;
                                        array_push($tableName,"bonus");
                                        array_push($tableValue,$point);
                                        $stringType .=  "s";
                                    }

                                    array_push($tableValue,$uid);
                                    $stringType .=  "s";
                                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                    if($passwordUpdated)
                                    {   }
                                    else
                                    {   }
                                }
                                else
                                {   }
                                $_SESSION['messageType'] = 1;
                                // header('Location: ../adminTransferRegisterPoint.php?type=3');
                                header('Location: ../adminDashboard.php?type=1');

                            }
                            else
                            {
                                $_SESSION['messageType'] = 1;
                                header('Location: ../adminTransferRegisterPoint.php?type=4');
                            }

                        }
                        else
                        {
                            $_SESSION['messageType'] = 1;
                            header('Location: ../adminTransferRegisterPoint.php?type=5');
                        }
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminTransferRegisterPoint.php?type=6');
                }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminTransferRegisterPoint.php?type=7');
        }

        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($pointBeingTransfer)
            {
                // array_push($tableName,"point");
                array_push($tableName,"bonus");
                array_push($tableValue,$pointBeingTransfer);
                $stringType .=  "s";
            }

            array_push($tableValue,$currentUsernameSearch);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {   }
            else
            {   }
        }
        else
        {   }

    }
    else
    {
        header('Location: ../index.php');
    }

function adminRegisterPoint($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status)
{
    if(insertDynamicData($conn,"register_point",array("send_uid","send_name","amount","receive_name","receive_uid","status"),
            array($uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status),"ssssss") === null)
        {
            return false;
        }
    else
    {  }
    return true;
    }
?>