<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        

        $uid = $_SESSION['uid'];

        $update_email = rewrite($_POST['update_email']);
        $icNo = rewrite($_POST["update_icno"]);
        $username = rewrite($_POST["update_username"]);
        $fullname = rewrite($_POST["update_fullname"]);
        $birthday = rewrite($_POST["update_birthday"]);
        $gender = rewrite($_POST["update_gender"]);
        $phoneNo = rewrite($_POST["update_phoneno"]);
        $address = rewrite($_POST["update_address"]);

        $bankName = rewrite($_POST["update_bankname"]);
        $bankAccountHolder = rewrite($_POST["update_bankaccountholder"]);
        $bankAccountNo = rewrite($_POST["update_bankaccountnumber"]);

        $carModel = rewrite($_POST["update_carmodel"]);
        $carYear = rewrite($_POST["update_caryear"]);


        // if($update_email == $oriEmail )
        // {echo "BLA BLA BLA";}
        // else{}
        //if($update_email = $userEmail)


        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");    

        //$user = getUser($conn," WHERE email = ? OR username = ? ",array("email", "username"),array($update_email, $username),"ss");

        //$user = getUser($conn," WHERE email = ? OR username = ? ",array("email", "username"),array($update_email, $username),"ss");
        
        //$passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$uid),"sss");
        if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($update_email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$update_email);
                $stringType .=  "s";
            }
            if($icNo)
            {
                array_push($tableName,"ic_no");
                array_push($tableValue,$icNo);
                $stringType .=  "s";
            }
            if($username)
            {
                array_push($tableName,"username");
                array_push($tableValue,$username);
                $stringType .=  "s";
            }
            if($fullname)
            {
                array_push($tableName,"full_name");
                array_push($tableValue,$fullname);
                $stringType .=  "s";
            }
            if($birthday)
            {
                array_push($tableName,"birthday");
                array_push($tableValue,$birthday);
                $stringType .=  "s";
            }
            if($gender)
            {
                array_push($tableName,"gender");
                array_push($tableValue,$gender);
                $stringType .=  "s";
            }
            if($phoneNo)
            {
                array_push($tableName,"phone_no");
                array_push($tableValue,$phoneNo);
                $stringType .=  "s";
            }
            if($address)
            {
                array_push($tableName,"address");
                array_push($tableValue,$address );
                $stringType .=  "s";
            }

            if($bankName)
            {
                array_push($tableName,"bank_name");
                array_push($tableValue,$bankName );
                $stringType .=  "s";
            }
            if($bankAccountHolder)
            {
                array_push($tableName,"bank_account_holder");
                array_push($tableValue,$bankAccountHolder );
                $stringType .=  "s";
            }
            if($bankAccountNo)
            {
                array_push($tableName,"bank_account_no");
                array_push($tableValue,$bankAccountNo );
                $stringType .=  "s";
            }

            if($carModel)
            {
                array_push($tableName,"car_model");
                array_push($tableValue,$carModel );
                $stringType .=  "s";
            }
            if($carYear)
            {
                array_push($tableName,"car_year");
                array_push($tableValue,$carYear );
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                $_SESSION['messageType'] = 1;
                header('Location: ../profile.php?type=1');
                // header('Location: ../editProfile.php?type=3');
            }
            else
            {
                // echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../editProfile.php?type=2');
            }
        }
        else
        {
            //echo "";
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=1');
        }

    }
else 
{
    //header('Location: ../editprofile.php');
    header('Location: ../index.php');
}
?>
