<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://qlianmeng.asia/adminTransferPoint.php" />
    <meta property="og:title" content="公司转让运输分数 | Q联盟" />
    <title>公司转让运输分数 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/adminTransferPoint.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

  <h1 class="username">转让运输分数</h1>
  <h3>分数 : <?php echo $userRefereeLimit = $userDetails->getPoint(); ?>分</h3>

    <form method="POST" action="utilities/adminICSearchFunction.php" >

    <div class="input-grey-div">
        <div class="left-points-div underline-div">
            <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="分数" title="分数"></span>
            <input class="login-input name-input clean" type="number" id="transferAmount" name="transferAmount" placeholder="转让分数">
        </div>
    </div>

    <div class="input-grey-div">
        <div class="left-points-div underline-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="用户名" title="用户名"></span>
            <input class="login-input name-input clean" type="text" id="username" name="username" placeholder="用户名">
        </div>
    </div>

    <!-- <input type="hidden" id="withdraw_epin_convert" name="withdraw_epin_convert" value="123321"> -->

    <div class="clear"></div>

    <button class="yellow-button clean" type="submit">转让</button>

    </form>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
